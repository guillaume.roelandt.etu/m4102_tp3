| Opération | URI                  | Action réalisée                               | Retour                                              |
|:----------|:---------------------|:----------------------------------------------|:----------------------------------------------------|
| GET       | /commandes           | récupère l'ensemble des commandes             | 200 et un tableau de commandes                      |
| GET       | /commandes/{id}      | récupère la commande d'identifiant id         | 200 et la commandes                                 |
|           |                      |                                               | 404 si id est inconnu                               |
| GET       | /commandes/{id}/name | récupère le nom de la commande                | 200 et le nom de la commande                        |
|           |                      | d'identifiant id                              | 404 si id est inconnu                               |
| POST      | /commandes           | création d'un commande                        | 201 et l'URI de la ressource créée + représentation |
|           |                      |                                               | 400 si les informations ne sont pas correctes       |
|           |                      |                                               | 409 si la commande existe déjà (même nom)           |
| DELETE    | /commandes/{id}      | destruction de la commande d'identifiant id   | 204 si l'opération à réussi                         |
|           |                      |                                               | 404 si id est inconnu                               |
