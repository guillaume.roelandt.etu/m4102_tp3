| Opération | URI               | Action réalisée                               | Retour                                              |
|:----------|:------------------|:----------------------------------------------|:----------------------------------------------------|
| GET       | /pizzas           | récupère l'ensemble des pizzas                | 200 et un tableau de pizzas                         |
| GET       | /pizzas/{id}      | récupère la pizza d'identifiant id            | 200 et la pizzas                                    |
|           |                   |                                               | 404 si id est inconnu                               |
| GET       | /pizzas/{id}/name | récupère le nom de la pizza                   | 200 et le nom de la pizza                           |
|           |                   | d'identifiant id                              | 404 si id est inconnu                               |
| POST      | /pizzas           | création d'un pizza                           | 201 et l'URI de la ressource créée + représentation |
|           |                   |                                               | 400 si les informations ne sont pas correctes       |
|           |                   |                                               | 409 si la pizza existe déjà (même nom)              |
| DELETE    | /pizzas/{id}      | destruction de la pizza d'identifiant id      | 204 si l'opération à réussi                         |
|           |                   |                                               | 404 si id est inconnu                               |
